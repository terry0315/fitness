angular.module('app').factory('workoutService', workoutService);
workoutService.$inject = ['$http', '$q', '$localStorage'];

function workoutService($http, $q, $localStorage) {
    const rest = {};

    rest.getWorkoutByPeriodAndGender = function getWorkoutByPeriodAndGender(period, gender) {
        const deferred = $q.defer();
        $http({
            method: 'GET',
            url: `https://thawing-spire-30271.herokuapp.com/api/workouts/${period}/${gender}`
        }).then((res) => {
            deferred.resolve(res);
        }, (err) => {
            deferred.reject(err);
        });
        return deferred.promise;
    };

    rest.saveWorkout = function saveWorkout(results) {
        $localStorage.workouts = results;
    };
    
    return rest;
}
