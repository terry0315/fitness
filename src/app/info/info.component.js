import tpl from './info.html';
import './info.less';

angular.module('app').component('info', {
    template: tpl,
    controller: infoCtrl,
    controllerAs: 'vm'
});

infoCtrl.$inject = ['$http', '$mdSidenav', '$state', 'workoutService'];
function infoCtrl($http, $mdSidenav, $state, workoutService) {
    const vm = this;
    // debugger;
    // workoutService.getWorkout().then((res) => {
    //     console.log(res);
    // }, (err) => {
    //     console.log(err);
    // });

    vm.gotoWorkout = function() {
        $state.go('workout');
    };

    vm.goback = function(routeName) {
        history.go(-1);
    };
};