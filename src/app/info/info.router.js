angular.module('app').config([
    '$urlRouterProvider',
    'stateHelperProvider',
    (
        $urlRouterProvider,
        stateHelperProvider
    ) => {
        stateHelperProvider.state({
            name: 'info',
            url: '/info/gender/:gender',
            component: 'info'
        },{ keepOriginalNames: true });
    }]);
