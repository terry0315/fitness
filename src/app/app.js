import angular from 'angular';
import '../style/app.less';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};

AppCtrl.$inject = [
  '$scope', 
  '$rootScope', 
  '$mdToast',
  '$state',
  '$localStorage'
];
function AppCtrl(
  $scope, 
  $rootScope, 
  $mdToast,
  $state,
  $localStorage
) {
  this.url = 'https://github.com/preboot/angular-webpack';

  this.go = function(){
    $state.go('home');
    console.log('hellllo');
  }

};  

AppConfig.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  'stateHelperProvider'
];
function AppConfig(
  $stateProvider, 
  $urlRouterProvider,
  stateHelperProvider
) {

  $urlRouterProvider.when(/^$/, '/home');
  $urlRouterProvider.otherwise('/home');
};

angular.module('app', [
  'ngMaterial',
  'ngAria',
  'ui.router',
  'ui.router.stateHelper',
  'ngMessages',
  'ngStorage'
])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl)
  .config(AppConfig);

// export default MODULE_NAME;