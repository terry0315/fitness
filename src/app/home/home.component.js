import tpl from './home.html';
import './home.less';

angular.module('app').component('home', {
    template: tpl,
    controller: homeCtrl,
    controllerAs: 'vm'
});

homeCtrl.$inject = ['$http', '$mdSidenav', '$state', '$localStorage'];
function homeCtrl($http, $mdSidenav, $state, $localStorage) {
    const vm = this;

    vm.toggleLeft = buildToggler('left');

    vm.person = {
        gender: 'M',
        goal: 'gainMuscle',
        period: '2'
    }

    vm.$onInit = function $onInit() {
        $localStorage.day = null;
    };


    vm.next = function () {
        console.log(vm.person.gender);
        $state.go('workout', {
            gender: vm.person.gender,
            goal: vm.person.goal,
            period: vm.person.period
        });
    };

    function buildToggler(componentId) {
        return function() {
          $mdSidenav(componentId).toggle();
        };
      }
};