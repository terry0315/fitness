import tpl from './workout.html';
import tpl2 from './workoutDetail.html';
import './workout.less';

angular.module('app').component('workout', {
    template: tpl,
    controller: workoutCtrl,
    controllerAs: 'vm'
}).component('workoutDetail', {
    template: tpl2,
    controller: workoutDetailCtrl,
    controllerAs: 'vm'
});

workoutCtrl.$inject = ['$http', '$mdSidenav', '$state', 'workoutService', '$stateParams', '$localStorage'];
function workoutCtrl($http, $mdSidenav, $state, workoutService, $stateParams, $localStorage) {

    const vm = this;
    vm.selectedIndex = null;

    // state params
    vm.period = $stateParams.period;
    var gender = $stateParams.gender;
    vm.goal = $stateParams.goal;
    // empty objects
    vm.workout = {};
    vm.day = {};
    // loading for spinner
    vm.loading = true;

    vm.$onInit = function $onInit() {
        // $localStorage.day = null;
        console.log($state.current.name);
        // get the key
        workoutService.getWorkoutByPeriodAndGender(vm.period, gender).then((res) => {
            // get the days
            vm.workout.day = Object.keys(res.data[0].exercises);
            console.log('vm.workout', vm.workout.day);
            console.log('vm.workout.day', vm.workout.day);

        }, (err) => {
            console.log(err);
        }).then(() => {
            // select first day
            // vm.day = '第一天';

            if($localStorage.day == null) {
                $localStorage.day = '第一天';
            }
            vm.day = $localStorage.day;

            if ($localStorage.day) {
                workoutService.getWorkoutByPeriodAndGender(vm.period, gender).then((res) => {
                    console.log('test workout', res.data[0].exercises[$localStorage.day]);
                    vm.workouts = res.data[0].exercises[$localStorage.day];
                    console.log('days', res.data[0].exercises);
                    vm.loading = false;
                    vm.error = false;
                }, (err) => {
                    console.log(err);
                    vm.loading = false;
                    vm.error = true;
                });
            }
        });
    };

    vm.selectDay = function () {
        console.log('chose', vm.day);
        $localStorage.day = vm.day;
        vm.loading = true;
        workoutService.getWorkoutByPeriodAndGender(vm.period, gender).then((res) => {
            console.log('workout', res.data[0].exercises[vm.day]);
            vm.workouts = res.data[0].exercises[vm.day];
            console.log('days', res.data[0].exercises);
            vm.loading = false;
            vm.error = false;
        }, (err) => {
            console.log(err);
            vm.loading = false;
            vm.error = true;
        });
    };

    vm.gotoDetail = function gotoDetail(index) {
        console.log('index', index);
        $state.go('workoutDetail', {
            id: index,
            gender: gender,
            period: vm.period,
            goal: vm.goal,
            day: vm.day
        });
    };

    vm.goback = function (routeName) {
        $state.go(routeName);
    };
};

workoutDetailCtrl.$inject = ['$http', '$mdSidenav', '$state', 'workoutService', '$stateParams', '$sce'];
function workoutDetailCtrl($http, $mdSidenav, $state, workoutService, $stateParams, $sce) {
    const vm = this;

    vm.id = $stateParams.id;
    vm.day = $stateParams.day;
    vm.period = $stateParams.period;
    vm.goal = $stateParams.goal;
    vm.gender = $stateParams.gender;

    vm.loading = true;

    console.log(vm.day);

    vm.$onInit = function $onInit() {
        workoutService.getWorkoutByPeriodAndGender(vm.period, vm.gender).then((res) => {
            console.log('test workout', res.data[0].exercises[vm.day][vm.id]);
            vm.workout = res.data[0].exercises[vm.day][vm.id];
            vm.desc = $sce.trustAsHtml(vm.workout.desc);

            vm.loading = false;
            vm.error = false;
        }, (err) => {
            console.log(err);
            vm.loading = false;
            vm.error = true;
        });
    };

    vm.goback = function (routeName) {
        history.back();
    };
};