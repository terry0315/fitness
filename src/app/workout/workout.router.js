angular.module('app').config([
    '$urlRouterProvider',
    'stateHelperProvider',
    (
        $urlRouterProvider,
        stateHelperProvider
    ) => {
        stateHelperProvider.state({
            // parent: 'home',
            name: 'workout',
            url: '/workout/gender/:gender/goal/:goal/period/:period',
            component: 'workout'
        },{ keepOriginalNames: true })
        .state({
            // parent: "home",
            name: 'workoutDetail',
            url: '/workout/gender/:gender/goal/:goal/period/:period/day/:day/id/:id',
            component: 'workoutDetail'
        },{ keepOriginalNames: true });
    }]);
