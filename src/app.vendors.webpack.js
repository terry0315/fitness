// This file is an entry point for angular tests
// Avoids some weird issues when using webpack + angular.

import 'angular';
import 'angular-mocks/angular-mocks';
import 'angular-material';
import 'angular-material/angular-material.css';
import 'angular-messages';
import 'angular-animate';
import 'angular-aria';
import 'angular-ui-router';
import 'angular-ui-router.statehelper';
import 'lodash';
import 'moment';
import 'angularfire';
import 'ngstorage';

const req = require.context('./app', true, /^(?!.*spec.js)((.*\.(js$))[^.]*$)/igm);
req.keys().forEach((key) => {
  req(key);
});
//shared folder
const directives = require.context('./services/', true, /^(?!.*spec.js)((.*\.(js$))[^.]*$)/igm);
directives.keys().forEach((key) => {
  directives(key);
});

const components = require.context('./components/', true, /^(?!.*spec.js)((.*\.(js$))[^.]*$)/igm);
components.keys().forEach((key) => {
  components(key);
});
